rm(list=ls())
library(dplyr)
library(ranger)
library(pROC)
library(lubridate)
library(jsonlite)
library(e1071)
source('./R/functions.r')

#install_git('git@bitbucket.org:dimalvov/kb.git',credentials=git2r::cred_ssh_key("~/.ssh/id_rsa.pub", "~/.ssh/id_rsa"))

df.raw <- read.csv('./../reports/bfx/rates.csv', stringsAsFactors = F)
colnames(df.raw) <- c('ask', 'ask_amt', 'bid', 'bid_amt', 'ts')

ts <- ymd_hms(as.character(df.raw$ts))
df.raw <- df.raw[ts>'2018-07-05',]
#df.raw <- df.raw[minute(df.raw$ts) %% 10 == 0,]

df <- df.raw %>% transformData()

#set the targets and df for which target is not NA
{
target <- df %>%
  #mutate(target.b = lead(bid,1) / ask ) %>%
  mutate(target.b = (
     (lead(bid,1) + lead(bid,2) + lead(bid,3) + lead(bid,4) + lead(bid,5)
     +lead(bid,6) + lead(bid,7) + lead(bid,8) + lead(bid,9) + lead(bid,10))/10
                      ) / ask > 1.002) %>%
  mutate(target.b = as.numeric(target.b)) %>% 
  select(one_of(c('target.b')))
}

df <- df[complete.cases(target),]
target <- target[complete.cases(target),]

test <- which(df$ts >= quantile(df$ts, 0.8))

df.train <- list(test = df[test,], train = df[-test,])

df.train$train <- df.train$train %>% 
  select(-one_of(c('bid', 'ask', 'ask_amt','bid_amt','ts')))

model <- ranger(target[-test] ~., data = df.train$train, 
                importance = 'impurity',num.trees  = 100, verbose = T)

#model <- lm(target[-test] ~., data = df.train$train)

advice.b <- predict(model, data = df.train$test)$predictions
#advice.b <- predict(model, newdata = df.train$test)

r <- roc(target[test], advice.b)
model$train_date <- as.character(Sys.time())
model$auc <- r$auc[1]

cor.test(advice.b, target[test], use='complete.obs')
#up

fisher.test(advice.b >= 1.00, target[test] >= 1.00)

hist(advice.b)

#here larning discovery happens
{
  #model <- ranger(df.target$target.b[-test] ~., data = df.train$train, num.trees = 100, verbose = T, importance = 'impurity')
  #advice.b <- predict(model, data = df.train$test)$predictions

  #optim <- wrap_my_cross_optimize_trade(step=0.05, advice.b, advice.s, 
  #                             ask=df$ask[test], bid=df$bid[test])


  
  #random predictor
  {
  #advice <- runif(length(df.train$test$target))
  
  #find the optimal threshold
  # thd<-optim(c(0.5,0.5),f = wrap_optimize_trade,
  #            advice.s=advice.s, advice.b=advice.b, ask=df$ask[test], bid=df$bid[test], 
  #            control=list(fnscale=-1, trace=6))
  # 
  # thd2<-optim(c(0.5,0.5),f = wrap_cross_optimize_trade, method="SANN",
  #           advice.s=advice.s, advice.b=advice.b, ask=df$ask[test], bid=df$bid[test], 
  #           control=list(fnscale=-1))
  }
  
  #nnet did not converge
  {
  #vars <- names(df$train[!(names(df$train) %in% c('target','hour','wday'))]) %>% paste(collapse='+')
  #vars <- as.formula(paste0('target~',vars))
  #vars <- c('bid_ma3', 'bid_ma10', 'bid_ma30', 'bid_ma60', 'bid_ma120',
  #          'ask_ma3','ask_ma10','ask_ma30','ask_ma60' ,'ask_ma120')
  #m2 <- neuralnet(as.formula(paste0('target~',paste(vars,collapse='+'))), 
  #                data = df$train, hidden = c(5,5,5), linear.output = FALSE)
  #advice <- compute(m2,covariate = df$test[,(names(df$train) %in% vars)])$net.result
  }
  
  #boost AUC=0.51
  {
    #gbm auc=0.51
    #m2.b <- gbm(df.target$target.b[-test]~., data = df.train$train, n.trees = 100)
    #advice.b <- predict(m2.b, data = df.train$test,n.trees=100)
    
    #m2 <- xgboost(label=df.train$train$target, data = data.matrix(df.train$train), nrounds = 100)
    #advice <- predict(m2, newdata = data.matrix(df.train$test))
  }
  
  #LR AUC=0.55
  {
  # m2.b <- glm(df.target$target.b[-test] ~., data = df.train$train)
  # advice.b <- predict(m2.b, data = df.train$test,type='response')
  }
}

# here we choose thresholds
{
#crosstrade(10, df$ask[test], df$bid[test], buy = advice.b >= 0.8, sell = advice.b < 0.3)
  
opt <- wrap_my_cross_optimize_trade(step = 0.01, 
                                    advice.b = advice.b, start = min(advice.b), end=max(advice.b),
                                    ask = df.train$test$ask, bid=df.train$test$bid)

persp(unique(opt$buy_if), unique(opt$sell_if), 
      matrix(opt$yield_mean, nrow=length(unique(opt$buy_if)), ncol=length(unique(opt$sell_if))),
      theta =45, phi = 30,  
      xlab = 'sell if score <', ylab= 'buy if score >', zlab = 'yield', shade=.0001, ticktype="detailed")

#opt <- opt[opt$buy_if > opt$sell_if,]

#save the model when we know the optimum thds
model$buy_if <- opt$buy_if[which.max(opt$yield_mean/sqrt(opt$yield_var))]
model$sell_if <- opt$sell_if[which.max(opt$yield_mean/sqrt(opt$yield_var))]

diaPlot(df=df.train$test, 
        buy=advice.b > model$buy_if,
        sell=advice.b < model$sell_if,
        target = target)
}

#or as simple as this?
#model$buy_if <- 1
#model$sell_if <- 1
opt[which.max(opt$yield_mean),]
opt[which.max(opt$yield_mean/sqrt(opt$yield_var)),]
max(opt$yield_mean / sqrt(opt$yield_var),na.rm = T)
model$buy_if
model$sell_if
save(model, file='./data/kbscore.rda')


