kbscore <- function(bidask){
  require(ranger)
  require(dplyr)
  t0 <- Sys.time()
  t_transform <- NA
  t_predict <- NA

  if(nrow(bidask) < 200){
    stop('kb expects at least 200 records')
  }
  
  if(length(intersect(c('ask', 'ask_amt', 'bid', 'bid_amt', 'ts'),colnames(bidask)))<5){
    stop('kb expects following vars: ask, ask_amt, bid, bid_amt, ts')
  }
  
  if(any(bidask$ask==0)|any(bidask$bid==0)){
    stop('kb expects ask and bid are not 0')
  }
  
  df.conv <- bidask %>% transformData() %>% filter(row_number() == n())
  ts <- as.character(as.POSIXct(df.conv$ts,origin='1970-01-01'))
  t_transform <- as.numeric(difftime(Sys.time(),t0,units = 'secs'))

  score <- predict(model, data=df.conv)$predictions
  t_predict <- as.numeric(difftime(Sys.time(), t0, units = 'secs'))

  score <- ifelse(length(score)>0, score, NA)
  ts <- ifelse(length(ts)>0,ts, NA)
  
  return(data.frame(
    score=score, 
    buy_if = model$buy_if, 
    sell_if = model$sell_if, 
    t_transform=t_transform, 
    t_predict=t_predict,
    model_auc = model$auc, 
    model_train = model$train_date,
    ask = df.conv$ask,
    bid = df.conv$bid,
    ts=ts
    ))
}

wrap_my_cross_optimize_trade <- function(step=0.05, advice.b, advice.s=advice.b, ask, bid, start=0, end=1){
  int <- end - start
  arr.res <- as.data.frame(matrix(nrow=floor((int/step))**2, ncol=8))
  colnames(arr.res) <- c('buy_if', 'sell_if', 'yield_mean', 'yield_var', 
                         'hold_time_mean', 'hold_mean', 'trade_length_mean', 'trades_mean')
  #debug
  i <- 1
  c <- 0
  while (i <= int/step ){
    j <- 1
    while (j <= int/step){
      c <- c + 1
      bsignal <- start + i*step
      ssignal <- start + j*step
      print(c(c, bsignal, ssignal))
      res <- crosstrade(times=10, ask, bid, buy = advice.b > bsignal, sell = advice.s < ssignal)
      arr.res$yield_mean[c] <- mean(res$yield)
      arr.res$yield_var[c] <- var(res$yield)
      arr.res$hold_time_mean[c] <- mean(res$mean_hold_period, na.rm = T)
      arr.res$hold_mean[c] <- mean(res$hold)
      arr.res$trade_length_mean[c] <- mean(res$trade_length)
      arr.res$trades_mean[c] <- mean(res$no_trades)
      arr.res$buy_if[c] <- bsignal
      arr.res$sell_if[c] <- ssignal
      j <- j + 1
    }
    i <- i + 1
  }
  arr.res
}

wrap_optimize_trade <- function(p=c(0,0), advice.b, advice.s, ask, bid){
  res <- trade(ask, bid, buy=advice.b>p[1], sell=advice.s<p[2], silent = T)
  res$yield
}

wrap_cross_optimize_trade <- function(p=c(0,0), advice.b, advice.s, ask, bid){
  res <- crosstrade(times=100, ask, bid, buy=advice.b>p[1], sell=advice.s>p[2])
  mean(res$yield)/var(res$yield)
}

crosstrade <- function(times, ask, bid, buy, sell, verbose=F, return=1,...){
  require(pbapply)
  require(dplyr)
  
  pres <- pblapply((1:times), function(x){
    start <- floor(runif(1)*length(buy-2)) # -2in order to have at least two points in run
    end <- sample((start+2):length(buy),size = 1)
    res <- trade(ask[start:end], bid[start:end], buy=buy[start:end], sell=sell[start:end], 
                 verbose = verbose, silent=T)
    res$trade_length = end-start
    res$iter <- x
    res
  }) 
  pres %>% bind_rows() %>% as.data.frame()
}

trade <- function(ask, bid, buy, sell=!buy, portf=1000, 
                  commision = 0.002, verbose = F, silent=F, stoploss=0,
                  takeprofit=2*stoploss) {
  
  portf_cash <- portf
  portf_crypto <- 0
  trades <- 0
  costs <- 0
  portf_crypto_cash <- portf
  
  run_total <- rep(NA,length(ask))
  pcr <- rep(NA,length(ask))
  pca <- pcr
  pc <- pcr
  
  buy.sig <-0
  sell.sig <-0
  
  for (i in 1:(length(buy)-1)){
    
    if (ask[i]==0 | bid[i] == 0) {
      next
    }
    
    #stoploss and takeprofit
    if((portf_crypto_cash < portf * (1-stoploss) 
        | portf_crypto_cash > portf * (1 + takeprofit))
       & portf_crypto > 0 & stoploss > 0){
      #copypaste - change to OOP style
      portf_cash <- portf_cash + (portf_crypto * bid[i]) - commision * (portf_crypto * bid[i])
      pc[i] <- commision * (portf_crypto * bid[i])
      portf_crypto <- 0
      trades <- trades + 1
    }

    if(buy[i]){

      if(portf_cash > 0){
        portf_crypto <- portf_crypto + (portf_cash / ask[i]) - commision * (portf_cash / ask[i])
        pc[i] <- commision*portf_cash
        portf_cash <- 0
        trades <- trades + 1
      }
    } else if(sell[i]){

      if(portf_crypto > 0){
        portf_cash <- portf_cash + (portf_crypto * bid[i]) - commision * (portf_crypto * bid[i])
        pc[i] <- commision * (portf_crypto * bid[i])
        portf_crypto <- 0
        trades <- trades + 1
      }
    }
    portf_crypto_cash = portf_crypto * bid[i] + portf_cash #current value of the portf
    yield = portf_crypto_cash/portf - 1
    hold = portf / ask[1] * bid[i+1]
    
    run_total[i] <- portf_crypto * bid[i] + portf_cash
    pcr[i] <- portf_crypto
    pca[i] <- portf_cash
    
  } 
  
  crypto_int <- rle(pcr>0)$lengths[which(rle(pcr>0)$values==1)] %>% mean(na.rm=T)
  cash_int <- rle(pcr>0)$lengths[which(rle(pcr>0)$values==0)] %>% mean(na.rm=T)
  costs <- pc %>% sum(na.rm=T)

  #diagnose strategy or get portf state on each period
  res <- list(
    cash=portf_cash, crypto=portf_crypto, total_funds = portf_crypto_cash, yield=yield,
    hold = hold,  fees_paid = costs,
    no_trades = trades, mean_hold_period = crypto_int, mean_cash_period = cash_int)
  
  if(!silent) print(res)
  
  if(verbose){
    invisible(cbind(buy, sell, run_total, yield, pca, pcr, pc))
  } else {
    invisible(res)
  }
   
}

getBreakThrough <- function(df, field, max_digits=10){
  
  f <- df[,field]
  
  digits <- max(nchar(round(f)))
  
  if(any(f < 1)){ #later
    stop('numbers < 1 are not yet implemented')
  }
  
  getDigit <- function(num, digit){
    res <- sapply(num, function(x) {
      pre <- as.character(floor(x)) %>% strsplit(.,'') %>% unlist()
      rev(pre)[digit]
      }
    )
    as.numeric(as.character(res))
  }
  
  res <- lapply(1:min(max_digits,digits), function(x){
    10^(x-1) * (getDigit(f, x) - getDigit(lag(f,1),x)) 
  }) %>% as.data.frame()
  
  colnames(res) <- paste(field, 'broke', 10^(1:min(max_digits,digits)-1), sep='_')
  
  cbind(df, res)
}

getLags <- function(df, field, n=c(1,2,3)){
  f <- df[,field]
  res <- lapply(n, function(x) {lag(f, n=x)/f}) %>% as.data.frame()
  colnames(res) <- paste0(field,'_','lag',n)
  cbind(df,res)
}

getMova <- function(df, field, n=c(1,2,3), dir='b'){
  f <- df[,field]
  if(dir=='b'){
    res <- lapply(n, function(x) {lag(f, n=x)}) %>% as.data.frame()
    res<-f/apply(res, 1, function(x) mean(x, na.rm=T)) %>% as.data.frame()
    colnames(res) <- paste0(field,'_','ma_b',max(n))
    cbind(df,res)} else
  if(dir=='f'){
      res <- lapply(n, function(x) {lead(f, n=x)}) %>% as.data.frame()
      res<-f/apply(res, 1, function(x) mean(x, na.rm=T)) %>% as.data.frame()
      colnames(res) <- paste0(field,'_','ma_f',max(n))
    }
}

getLpredict <- function(df, field, n=5, mod='b'){
  f <- df[,field]
  res <- rep(NA, length(f))
  pval <- res
  
  if(mod=='b'){
    for (i in n:length(f)){
      y = f[(i-n+1):i]
      x = seq((i-n+1):i)
      regr <- lm(y~x) %>% summary()
      res[i] <- coef(regr)[2,1]
      pval[i] <- coef(regr)[2,4]
    } 
  } else {
      for (i in 1:(length(f)-n)){
        y = f[i:(i+n-1)]
        x = seq(1:n)
        regr <- lm(y~x) %>% summary()
        res[i] <- coef(regr)[2,1]
        pval[i] <- coef(regr)[2,4]
      }
  }
  
  # out = data.frame(res,pval)
  # colnames(out) <- c(paste0('lpredict_',field,'_',n),
  #                             paste0('lpredict_p_',field,'_',n))
  
  out = data.frame(res)
  colnames(out) <- c(paste0('lpredict_',field,'_',n))
  cbind(df, out)
}

transformData <- function(df){
  df %>%
    filter(ask > 0) %>% 
    mutate(ts = as.POSIXct(ts,tz='GMT')) %>% 
    arrange(ts) %>% 
    mutate(hour = as.numeric(strftime(ts,'%H'))) %>% 
    mutate(wday = as.numeric(strftime(ts,'%u'))) %>% 
    getLags(., 'ask', (1:10)) %>% 
    getLags(., 'ask_amt', (1:10)) %>% 
    getLags(., 'bid', (1:10)) %>% 
    getLags(., 'bid_amt', (1:10)) %>% 
    mutate(spread = abs(ask - bid)/ask) %>%
    mutate(step = (bid-lag(ask,1))/bid) %>%
    #getDepth(.,'depth') %>% 
    #getLags(., 'step',(1:10)) %>% 
    getLags(., 'spread',(1:10)) %>% 
    getMova(., 'bid', (1:3)) %>% 
    getMova(., 'bid', (1:10)) %>% 
    getMova(., 'bid', (1:30)) %>% 
    getMova(., 'bid', (1:60)) %>% 
    getMova(., 'bid', (1:120)) %>% 
    getMova(., 'bid', (1:180)) %>%
    getMova(., 'ask', (1:3)) %>% 
    getMova(., 'ask', (1:10)) %>% 
    getMova(., 'ask', (1:30)) %>% 
    getMova(., 'ask', (1:60)) %>% 
    getMova(., 'ask', (1:120)) %>%
    getMova(., 'ask', (1:180)) %>%
    getMova(., 'bid_amt', (1:3)) %>% 
    getMova(., 'bid_amt', (1:10)) %>% 
    getMova(., 'bid_amt', (1:30)) %>% 
    getMova(., 'bid_amt', (1:60)) %>% 
    getMova(., 'bid_amt', (1:120)) %>%
    getMova(., 'ask_amt', (1:3)) %>% 
    getMova(., 'ask_amt', (1:10)) %>% 
    getMova(., 'ask_amt', (1:30)) %>% 
    getMova(., 'ask_amt', (1:60)) %>% 
    getMova(., 'ask_amt', (1:120)) %>%
    getMova(., 'spread', (1:3)) %>% 
    getMova(., 'spread', (1:10)) %>% 
    getMova(., 'spread', (1:30)) %>% 
    getMova(., 'spread', (1:60)) %>% 
    getMova(., 'spread', (1:120)) %>%
    getMova(., 'step', (1:3)) %>% #this yields Inf sometimes
    getMova(., 'step', (1:10)) %>% 
    getMova(., 'step', (1:30)) %>% 
    getMova(., 'step', (1:60)) %>% 
    getMova(., 'step', (1:120)) %>%
    getBreakThrough(.,'ask') %>% 
    getBreakThrough(.,'bid') %>% 
    mutate_all(funs(ifelse(.==Inf, NA,.))) %>% 
    mutate_all(funs(ifelse(.==-Inf, NA,.))) %>% 
    na.omit()
}

parRf <- function(df, target, ntree=500, ...){
  require(parallel)
  require(pbapply)
  require(randomForest)
  c <- parallel::makeCluster(parallel::detectCores()-1, type = 'FORK')
  print(c)
  tryCatch({
  models <- pblapply(as.list(1:ntree), function(x) {randomForest(target~., data=df, ntree=1)}, cl=c)
  error = function(e) print(e)
  })
  parallel::stopCluster(c)
  c <- c()
  m <- do.call("combine", models) 
  return(m)
}

diaPlot <- function(df, buy, sell, target, n=sample(nrow(df),1), m=n+200){
  plot(df$ask[n:m], type='l', 
       xlab=paste(as.POSIXct(min(df$ts[n:m]),origin="1970-01-01"),
                 as.POSIXct(max(df$ts[n:m]),origin="1970-01-01"),sep=' to '))
  lines(df$bid[n:m], col='blue')
  #points(df$ask[n:m], pch=18, col=target[n:m]+1)
  points(df$ask[n:m]*buy[n:m], pch=18, col='green')
  points(df$bid[n:m]*sell[n:m], pch=18, col='red')
}

getDepth <- function(df, field) {
  #a function that gets df (grigorys rates) as argument and returns for each row an set of depth parameters.
  #works by lapplying anadepth to each value of the column with depth 
  
  f <- df[,field]
  df[,field] <- NULL
  
  res <- do.call('rbind',lapply(f, anaDepth))
  cbind(df,res)
}

anaDepth <- function(json) {
  require(e1071)
  d <- json %>% 
    fromJSON() %>% 
    as.data.frame()
  
  d <- d[1:25,] %>% 
    mutate_all(as.character) %>% 
    mutate_all(as.numeric)
  
  #(hopefully) smart depth vars:
  s__meanSellToBuyVol <- mean(d$sellVolume) / mean(d$buyVolume)
  s__medianSellToBuyVol <- median(d$sellVolume) / median(d$buyVolume)
  s__skewSellPrice <- skewness(d$sellPrice)
  s__skewBuyBuyPrice <- skewness(d$buyPrice)
  s__kurtosisSellPrice <- kurtosis(d$sellPrice)
  s__kurtosisSellPrice <- kurtosis(d$sellPrice)
  
  mget(ls()[grep('s__', ls())]) %>% as.data.frame()
}
